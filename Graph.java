import java.util.*;
import java.lang.Character;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/* there are a few assumptions for the input data
 * 1) host name is a single char and continuous, i.e. A, B, C...
 * 
 * */
public class Graph {
	//Total number of hosts
	private int numOfHosts;
	//Total number of links
	private int numOfLinks;
	//2D ArrayList is used to save the graph structure
	private ArrayList< ArrayList<GraphNode> > graph;

	//Graph node class
	private class GraphNode {
		//host is saved as int instead of char so that we can use it directly as index in ArrayList
		private int host;
		//protocol from previous host to this host is also saved
		private String	protocolToHost;
		
		public GraphNode(int host) {
			if(  host<0 )
				throw new IllegalArgumentException("Node Host is invalid");
			
			this.host = host;
			this.protocolToHost = "ftp";
		}
		
		public GraphNode(int host, String protocol) {
			if(  host<0 || protocol==null )
				throw new IllegalArgumentException("Node Host or Protocol name is invalid");
				
			this.host = host;
			this.protocolToHost = protocol;
		}	
		
		public int getHost(){
			return this.host;
		}
		
		public String
		getProtocolToHost(){
			return this.protocolToHost;
		}
	}
	
	//Constructor
	public Graph(){
		numOfHosts = 0;
		numOfLinks = 0;
		graph = new ArrayList< ArrayList<GraphNode> >();
	}
	
	//Add a link to the graph, it can
	//1) construct the graph for the first time
	//2) add new links once the graph has been constructed
	public void addLink(int startHost, String protocol, int endHost)
	{
		if( startHost < 0 || endHost < 0 || protocol ==null || startHost == endHost)
			throw new IllegalArgumentException("Start host name is invalid");
		
		//Create a new node
		GraphNode node = new GraphNode(endHost,protocol);
	
		//update the largest host value, i.e. number of host, b/c we assume there is no skip in host naming
		numOfHosts = (numOfHosts < startHost+1) ? (startHost+1) :  numOfHosts;
		numOfHosts = (numOfHosts < endHost+1) ? (endHost+1) :  numOfHosts;
		
		//if current graph doesn't have the start host
		if( graph == null || startHost >= graph.size() ){	
			
			//This is to put fillers and loop to the right position
			for(int i = graph.size() ; i < startHost; ++i){
				//Add place holder only
				ArrayList<GraphNode> adjNodes = new ArrayList<GraphNode>();
				graph.add(adjNodes);
			}
			
			//Create an ArrayList for adjacent nodes, i.e. adjNodes that holds the newly added node (and protocol)
			ArrayList<GraphNode> adjNodes = new ArrayList<GraphNode>();
			adjNodes.add(node);
			//Add the startHost to the right location
			graph.add(adjNodes);
			numOfLinks++;			
		}
		else{//If startHost already exists in the graph
			
			if(graph.get(startHost) == null ){
				throw new IllegalArgumentException("Cannot locate start host");
			}
			//if a link to endHost already exists, throw error
			for(int i = 0; i <graph.get(startHost).size(); ++i ){
				if( this.graph.get(startHost).get(i).getHost() == endHost ){
					throw new IllegalArgumentException("Link already exists");
				}//end if					
			}//end for
			
			graph.get(startHost).add(node);
			numOfLinks++;							
		}			
	}
	
	//Breath first search to find the easiest way if there is one and print result
	public void printEasiestWay(int start, int finish){
		//Stack is used for reversing the found easiest way
		Stack<GraphNode> easiestWay = new Stack<GraphNode>();
		
		//Sanity check
		if(numOfHosts<=0)
			throw new IllegalArgumentException("Invalid number of hosts");
		if( start >=  numOfHosts || finish >= numOfHosts)
			throw new IllegalArgumentException("Invalid hosts");
		
		//mark if the node has been visited
		boolean[] visited = new boolean[numOfHosts];
		//previous node to the current one, used for tracing back
		Map<GraphNode,GraphNode> previous = new HashMap<GraphNode,GraphNode>();
		//Queue used for breath first search
		Queue<GraphNode> queue = new LinkedList<GraphNode>();
		
		GraphNode currentNode = new GraphNode(start);
		queue.add(currentNode);
		visited[currentNode.getHost()] = true;
		//Breath first search
		while( !queue.isEmpty() ){
			currentNode = queue.remove();
			//Found a path to the finish node
			if( currentNode.getHost() == finish ){
				break;
			}else{
				int currentIndex = currentNode.getHost();
				if(graph.get( currentIndex )!=null){//Sanity check
					for(int i = 0; i <graph.get(currentIndex).size(); ++i ){ // for all adjacent nodes
						int nextIndex = graph.get(currentIndex).get(i).getHost();
						if( nextIndex >=0 && nextIndex < numOfHosts && !visited[nextIndex] ){
							GraphNode nextNode = graph.get(currentIndex).get(i);
							queue.add(nextNode);
							visited[nextIndex] = true;
							previous.put(nextNode, currentNode);
						}
					}
				}
			}
		}//end while
		
		//If search ends and no path can be found to the finish node
		if( currentNode.getHost() != finish ){
			System.out.println(indexToHostName(start)+" None "+indexToHostName(finish));
			return;
		}
		
		//There exists an easiest way  to the finish node
		//The way is reversed, use stack to get the correct order
		for(GraphNode n = currentNode; n != null; n = previous.get(n)){
			easiestWay.push(n);
		}
		
		//Construct the easiest way string for output
		StringBuilder wayString = new StringBuilder();
		wayString.append(indexToHostName(start));
		easiestWay.pop();
		while(!easiestWay.isEmpty())
		{
			GraphNode top = easiestWay.pop();
			wayString.append( " " + top.getProtocolToHost() + " " + indexToHostName(top.getHost()) );
		}
		
		System.out.println(wayString.toString());
		return;
	}

	//Print all links in the graph
	public void printLinks()
	{
		if(graph != null){
			for(int i = 0; i < graph.size(); ++i){
				if(graph.get(i)!=null){
					for(int j = 0;j<graph.get(i).size();++j){
						System.out.println( indexToHostName(i)  + " " + graph.get(i).get(j).getProtocolToHost() + " " + indexToHostName( graph.get(i).get(j).getHost() ));
					}//end for
				}//end if
			}//end for
		}//end if
	}
		
	//Print all hosts
	public void printHosts()
	{
		for(int i = 0; i<numOfHosts;++i)
			System.out.println( indexToHostName(i) );			
	}
	
	//****Private Methods****
	//convert host name(char) to index(int)
	private static int hostIndex(char host){
		if( !isValidHostName(host) )
			throw new IllegalArgumentException("Start host name is invalid");
		
		return charToIndex(host);
	}
	//check if host name is valid
	private static boolean isValidHostName(char host){
		if( (host >='a' && host<='z') || (host >='A' && host<='Z') )
			return true;
		else
			return false;
	}
	
	//Convert char to integer, which will be used as index
	private static int charToIndex(char c){
		
		char upper = Character.toUpperCase(c);
		return Character.getNumericValue(upper) - Character.getNumericValue('A'); 
	}
	
	//Convert host index(int) to host name(char) 
	private static char indexToHostName(int index){
		if(index < 0)
			throw new IllegalArgumentException("Invalid host index value");
		
		char res = (char)(index + 'A');
		return res;			
	}
	
	
	public static void main(String args[]){
	
		String psvFile = args[0];
		BufferedReader buffer = null;
		String line = "";
		String pipe = "\\|";
		
		try{
			  buffer = new BufferedReader(new FileReader(psvFile));
			  Graph graph = new Graph();
			  
			  //Load links from file and construct the graph
			  while ((line = buffer.readLine()) != null) {
	                // use pipe as separator
	                String[] link = line.split(pipe);

	                if(link.length != 3)
	                	throw new IllegalArgumentException("Invalid link format");
	                
	                int startHost = hostIndex(link[0].charAt(0));
	                int endHost = hostIndex(link[2].charAt(0));
	                
	                graph.addLink(startHost, link[1]/*protocol*/, endHost );
	            }
			  
			  //Print prompts and take inputs from User
			  System.out.println("Usage:\n"
			  		+ "1. Display all hosts\n"
			  		+ "2. Display all links\n"
			  		+ "3. Query the easiest way\n"
			  		+ "4. Add a new link\n"
			  		+ "5. Quit\n");
			
			  Scanner scan = new Scanner(System.in);
			  boolean quit = false;
			  while(quit == false){
				  System.out.println("Please enter a number 1-5\n");
				  int option = 0;
				  option = scan.nextInt();
				  
				  switch(option)
				  {
					  case 1: //Print all hosts
						  graph.printHosts();
						  break;
					  case 2: //Print all Links
						  graph.printLinks();
						  break;
					  case 3: //Query the easiest way between hosts
						  System.out.println("Key in Start Host\n");
						  String startHost = scan.next();
						  System.out.println("Key in End Host\n");
						  String endHost = scan.next();
						  
						  if( isValidHostName(startHost.charAt(0)) && 
								  isValidHostName(endHost.charAt(0)) )
								  graph.printEasiestWay(hostIndex(startHost.charAt(0)), hostIndex(endHost.charAt(0)));
							  else
								  System.out.println("Please enter valid host names\n");
						  break;
					  case 4: //Add new links
						  System.out.println("Key in Start Host\n");
						  String linkStartHost = scan.next();
						  System.out.println("Key in Transfer Protocol\n");
						  String linkProtocol = scan.next();
						  System.out.println("Key in End Host\n");
						  String linkEndHost = scan.next();
						  
						  if( isValidHostName(linkStartHost.charAt(0)) && 
								  isValidHostName(linkEndHost.charAt(0)) )
							  graph.addLink(hostIndex(linkStartHost.charAt(0)), linkProtocol, hostIndex(linkEndHost.charAt(0)));
						  else
							  System.out.println("Please enter valid host names\n");
						  break;
					  case 5:
						  quit = true;
						  System.out.println("Bye\n");
						  break;
					  default:
						  System.out.println("Please enter a valid number between 1 and 5");
						  break;
					  
				  }
			  }			  

		}catch (Exception  e) {
			if (buffer != null) {
                try {
                	buffer.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        } 
	}	
}
